[//]: # (Filename: Artesanía_de_software.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 27 sep 2020 22:47:56)
[//]: # (Last Modified: 28 sep 2020 18:42:51)

# Artesanía de software

Ser un genio hacker ayuda. Tener un plano bien diseñado de su software ayuda.
Tener un proceso dinámico orientado al cliente también ayuda. Pero en el
trasfondo de las cosas, se encuentra el trabajo que hacemos frente a la
computadora. Reconocer este trabajo es la base de una disciplina llamada
**artesanía de software** o «software craftsmanship». La *artesanía del
software* establece que una gran parte del desarrollo, lo que conocemos como
programación en sí, consiste en tareas simples que deben realizarse. Para
hacerlo bien, necesitamos tener las herramientas correctas, necesitamos tener
las habilidades correctas, y necesitamos aplicar ambas en la práctica. El hecho
de que la programación sea un oficio como la albañilería, la carpintería o la
confitería sugiere que:

* *El propósito es importante.* Creamos programas y aplicaciones como medios
  para un fin. La programación no es un arte; deseamos escribir programas que
  sean utilizados.
* *La planificación es importante.* Es una parte útil y necesaria del trabajo
  (medir dos veces, cortar una vez).
* *Las herramientas son importantes.* Tenemos que cuidar nuestro conjunto de
  herramientas y mantener nuestro lugar de trabajo en buen estado. El oficio
  nos ayuda a elegir las herramientas adecuadas.
* *Las habilidades son importantes.* Trabajamos en la mejora continua de
  nuestro oficio. Nos esforzamos por programar lo mejor posible, y al mismo
  tiempo reconocemos que nuestras habilidades no son perfectas.
* *La comunidad es importante.* Existe una gran comunidad de personas con ideas
  afines que honran el oficio. La comunidad es un lugar para aprendices y
  maestros por igual.
* *El tamaño no es importante.* No nos limitamos a un cierto tipo de proyecto.
  La puesta en práctica de nuestro oficio es necesario tanto si escribimos un
  programa de cinco líneas como si contribuimos a un proyecto enorme.
* *La práctica es importante.* No podemos resolver problemas de programación
  utilizando únicamente una pizarra o una hoja de cálculo. Para poder programar
  con éxito, necesitamos ensuciarnos las manos.
