---
title: "Acerca de"
layout: page
permalink: /acerca-de/
---

[//]: # (Filename: acerca-de.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 22 sep 2020 11:27:08)
[//]: # (Last Modified: 22 sep 2020 13:37:34)

Fundada en el año 2020, **Culture & Technologies** es una *startup* mexicana
que, como su nombre indica, tiene como principal misión el *difundir la cultura
y avances tecnológicos* a las empresas y comunidad hispana, a través de
servicios de enseñanza a distancia, consultoría y gestión técnica.

Si desea conocer más sobre nosotros, le invitamos cordialmente a que visite
nuestra [diapositiva][diapositiva] en donde encontrará información relevante
acerca de quienes somos, nuestros objetivos, así como también, una breve
descripción de los productos y servicios que ofrecemos.

[diapositiva]: ../slide/
