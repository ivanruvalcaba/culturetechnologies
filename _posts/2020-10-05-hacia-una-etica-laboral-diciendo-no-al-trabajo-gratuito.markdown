---
layout: post
title: "Hacia una ética laboral: diciendo no al trabajo gratuito"
date: "2020-10-04 09:14:48 -0500"
author: "César Ruvalcaba"
categories: ["Opinión"]
tags: ["Cultura", "Tecnología"]
excerpt: "He presenciado error tras error, mendicidad tras mendicidad, mal consejo sobre mal consejo. No puede ser que en pleno siglo XXI y con la tecnología que nos envuelve, haya quienes no comprendan el valor ético del trabajo y regalen todo. Regalan la asesoría y/o consultoría, obsequian la gestión, avientan el libro de su autoría. Al final de cuentas, ¿para qué tanto estudio si se iba a terminar como empleado. Pues la lógica es sencilla: ALGO POR LO QUE EL CLIENTE NO PAGA NO LO VALORA. Si te piden una asesoría, consultoría, revisión, gestión, trabajo técnico, terapia o demás tareas, cóbralas, ¡y cóbralas bien. Si eres pastor o sacerdote y tanto te has preparado para ello, cobra, y cobra bien, pues la Biblia te permite vivir de los frutos de la fe. Ahora les pregunto: ¿les siguen quedando ganas de regalar su trabajo cuando nadie te regala nada?"
comments: false
---

[//]: # (Filename: 2020-10-05-hacia-una-etica-laboral-diciendo-no-al-trabajo-gratuito.markdown)
[//]: # (Author: César Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 05 oct 2020 12:24:36)
[//]: # (Last Modified: 05 oct 2020 13:02:55)

He presenciado error tras error, mendicidad tras mendicidad, mal consejo sobre
mal consejo. No puede ser que en pleno siglo XXI y con la tecnología que nos
envuelve, haya quienes no comprendan el valor ético del trabajo y regalen todo.
Regalan la asesoría y/o consultoría, obsequian la gestión, avientan el libro de
su autoría. Lo único que ha producido ésta tendencia es mayor pobreza y
desprestigio, así como una desvalorización por parte de la sociedad, todo
debido a un mal concepto o creencias erróneas.

Está comprobado que para recuperar lo invertido en una profesión se requieren
alrededor de 8 a 10 años de trabajo, pues se toman en cuenta factores como si
se estudió en una universidad pública o privada, el tiempo que dura la
profesión elegida, los libros y materiales que se invierten (tómense en cuenta
carreras como ingenierías, gastronomía, medicina, odontología o arquitectura),
el costo de pasajes y/o recargas de gasolina, las gestorías, y sobretodo, el
pago de la titulación que ronda entre los 15 a 30 mil pesos. El recién graduado
se topa con el dilema de que ha crecido el desempleo y que no le queda otra
opción más que terminar laborando para una empresa negrera con un giro para
nada afín a lo que estudió (considerar que la gran mayoría de profesores solo
perfilan a los alumnos para ser excelentes empleados y no les motivan al
emprendimiento). En esta etapa, pueden ir descubriendo que la mejor opción
hubiese sido quedarse como pasante (ya que hay pasantes que ganan más que un
titulado) o solo concluir el nivel medio superior. Al final de cuentas, ¿para
qué tanto estudio si se iba a terminar como empleado?  Lo lamentamos por los
padres de familia que gastaron lo inimaginable.

Yo considero que ya no es tiempo para estar regalando nuestro trabajo.  ¿Qué
ocurre cuando lo hacemos? Pues la lógica es sencilla: ALGO POR LO QUE EL
CLIENTE NO PAGA NO LO VALORA. Un trabajo que cuesta nada, vale nada.

Si te piden una asesoría, consultoría, revisión, gestión, trabajo técnico,
terapia o demás tareas, cóbralas, ¡y cóbralas bien! Eso es parte de una ética
de trabajo. Igualmente pasa en el sector religioso.  Si eres pastor o sacerdote
y tanto te has preparado para ello, cobra, y cobra bien, pues la Biblia te
permite vivir de los frutos de la fe. No hagas caso a la gente envidiosa y
pusilánime que ni crece ni cobra ni quiere que crezcas ni que cobres para que
haya ya dos mediocres en la fila. Si el otro no quiere cobrar por su trabajo ni
crecer, tú sí cobra y crece. El dinero no lo es todo, pero no se puede negar
que todos los mecanismos del mundo hoy dependen del sistema económico y nadie
vive solo del aire, muchos menos, los pastores y sacerdotes.

Para los profesionistas jóvenes, les hago la invitación de que valoren el
esfuerzo de sus padres y el de ustedes, lo que implicó su formación, los años,
los desvelos, las privaciones alimentarias, los ritmos acelerados. Ahora les
pregunto: ¿les siguen quedando ganas de regalar su trabajo cuando nadie te
regala nada?
