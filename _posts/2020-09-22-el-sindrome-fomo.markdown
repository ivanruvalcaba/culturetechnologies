---
layout: post
title: "El síndrome FOMO"
date: "2020-09-22 12:56:27 -0500"
author: "César Ruvalcaba"
categories: ["Psicología"]
tags: ["Redes sociales", "Sociedad"]
excerpt: "En el caso del FOMO, hablamos de una sensación que experimenta una persona de quedarse fuera o ser excluida de algún evento social. Se manifiesta, de acuerdo a las investigaciones recientes, en jóvenes de entre 18 y 29 años de edad. Al encontrarlo, o peor aún al deducirlo sin elementos reales, genera un complejo de inferioridad, ansiedad y depresión. Las relaciones amistosas pueden llegar a destruirse por las erróneas apreciaciones de quien padece FOMO."
comments: false
---

[//]: # (Filename: 2020-09-22-el-sindrome-fomo.markdown)
[//]: # (Author: César Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 22 sep 2020 12:59:07)
[//]: # (Last Modified: 05 oct 2020 12:50:15)

Debido a la expansión impresionante de las redes sociales, no solo han sido
afectadas nuestras actividades cotidianas, sino también nuestra psicología.
Hoy trataremos sobre un nuevo síndrome reconocido como FOMO.

El nombre se integra por siglas en inglés (Fear of missing out) que se puede
traducir como "miedo de perderse algo". En el caso del FOMO, hablamos de una
sensación que experimenta una persona de quedarse fuera o ser excluida de algún
evento social. Se manifiesta, de acuerdo a las investigaciones recientes, en
jóvenes de entre 18 y 29 años de edad.

Quien padece dicho síndrome confiesa sentir preocupación por no pertenecer o
ser excluido de alguna reunión de amigos o eventos importantes en los cuales
desea pertenecer. Tiene temor a que no le hagan caso y los demás la pasen bien
sin su presencia. Por ende, recurre al mecanismo de conectarse a las redes
sociales todo el tiempo posible (incluso todo el día y la noche) para rastrear
cualquier indicio que presuma un acontecimiento grupal relevante. Al
encontrarlo, o peor aún al deducirlo sin elementos reales, genera un complejo
de inferioridad, ansiedad y depresión. Las relaciones amistosas pueden llegar a
destruirse por las erróneas apreciaciones de quien padece FOMO.

Al igual que una persona celotípica (dícese de alguien que sufre de celos
extremos) puede crear escenarios para "tener la razón" de sus sospechas, quien
padece FOMO puede inventar contextos y formulaciones. Los psicólogos concuerdan
en que este síndrome genera irracionalidad o distorsión cognitiva.  En casos
radicales, es menester acudir a terapia psicológica, pues el FOMO puede
agudizar la depresión y propiciar tentativas suicidas.

Por último, hay que mencionar que afecta en mayor porcentaje a los hombres.
