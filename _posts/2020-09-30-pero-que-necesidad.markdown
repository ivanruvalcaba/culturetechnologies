---
layout: post
title: "¿Pero que necesidad?"
date: "2020-09-30 13:24:14 -0500"
author: "César Ruvalcaba"
categories: ["Opinión"]
tags: ["Tecnología", "Sociedad"]
excerpt: "Como experto en Ciencias Sociales, no niego el sentirme preocupado por esta aversión social hacia los avances tecnológicos. ¿Qué fue lo que pasó con el equipo electrónico médico necesario para atender a los enfermos por este virus que nos azotó. Pero, ¿qué necesidad de pasar todo esto?, que se pudo haber evitado con un poco de visión y voluntad."
comments: false
---

[//]: # (Filename: 2020-09-30-pero-que-necesidad.markdown)
[//]: # (Author: César Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 30 sep 2020 13:29:50)
[//]: # (Last Modified: 05 oct 2020 12:48:35)

El fenómeno tecnológico y sus efectos expansivos han sido del agrado de un
sector y del disgusto de otro. Estoy convencido que si algo nos ha permitido
diagnosticar el advenimiento de esta pandemia del Covid-19 es que gran parte de
la sociedad mundial no estaba preparada para estos cambios, ni mucho menos,
para sacrificar las antiguas rutinas que, indubitablemente, ya requerían un
cambio con urgencia.

Como experto en Ciencias Sociales, no niego el sentirme preocupado por esta
aversión social hacia los avances tecnológicos. Veamos desde la esfera política
que las autoridades respectivas no invierten lo suficiente ni incentivan el
desarrollo tecnológico, llegando a priorizar el uso de resorteras en lugar de
invertir en dispositivos electrónicos que propicien un trabajo más eficiente y
que desgaste menos la salud humana. Aquí podemos identificar que el uso
tecnológico no siempre es maligno y que inclusive se le ha satanizado por miedo
a la tarea de actualización.

¿Qué fue lo que pasó con el equipo electrónico médico necesario para atender a
los enfermos por este virus que nos azotó?

A decir verdad, se le hizo frente a una enfermedad devastadora con aparatos ya
obsoletos, todo por no invertir, por seguir hilando la cortina de la
corrupción, y estoy seguro que la problemática de salud pública sigue siendo un
objetivo secundario para muchas instituciones.  Pero, ¿qué necesidad de pasar
todo esto, que se pudo haber evitado con un poco de visión y voluntad?

Ahora tenemos en el escritorio otro reto: LA EDUCACIÓN DE LOS NIÑOS Y DE LOS
PADRES. Pues, ahora, tanto padres como hijos deben ponerse a estudiar (labor
que los padres evitan pues por eso trabajan). Ya no podrán seguir aventando a
sus vástagos al colegio ni arrumbarlos como solían hacerlo. Los padres de
familia son como bombas de tiempo. La tecnología tiene tanto por apoyar para
aligerar la carga. Solo se requiere lo mismo que al principio: un poco de
interés.

¿La habrá?
