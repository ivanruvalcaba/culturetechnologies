---
layout: post
title: "La pobreza y la riqueza es una cuestión mental"
date: "2020-10-05 12:32:12 -0500"
author: "César Ruvalcaba"
categories: ["Psicología"]
tags: ["Sociedad"]
excerpt: "Tú eres lo que piensas de ti mismo, y de forma autónoma, eres digno por el hecho de ser humano. Somos seres únicos en este mundo y tenemos un propósito por cumplimentar. Por lo tanto, la riqueza y la pobreza son situaciones que nos creamos precisamente en ese sentido del pensamiento. Podemos sentirnos ricos sin tener grandes cosas o sentirnos pobres teniéndolo todo. Hay gente feliz gozando de lo suficiente, otros, son terriblemente infelices buscando entre sus propiedades algo que les genere placer."
comments: false
---

[//]: # (Filename: 2020-10-05-la-pobreza-y-la-riqueza-es-una-cuestion-mental.markdown)
[//]: # (Author: César Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 05 oct 2020 12:35:02)
[//]: # (Last Modified: 05 oct 2020 12:39:52)

¿Crees que la riqueza real consiste en tener muchas cosas materiales?

¿Crees que la pobreza real consiste en no tener muchas cosas materiales?

Si respondes que sí a ambas preguntas, déjame decirte que estás muy equivocado.
Los efectos del sistema ultracapitalista nos han hecho creer que la gente vale
por lo que tiene y no por lo que es, o mucho menos, por lo que piensa. Tú eres
lo que piensas de ti mismo, y de forma autónoma, eres digno por el hecho de ser
humano. (Esto lo he precisado cabalmente en mi libro célebre "Sobre la Dignidad
Humana")

Las personas valemos mucho por dos motivos: una, porque somos seres con
dignidad, y dos, porque contamos con las capacidades más elevadas del universo.
Solo el ser humano puede razonar, crear, patentar, forjar un destino. Ningún
animal puede hacerlo, ni un planeta, ni una galaxia, ni un meteorito, ni el
sol, ni la luna, ni las plantas pueden razonar y crear como lo hace la raza
humana. Si alguna vez perdiste el rumbo de tu vida, aprópiate de éstas
palabras.

El ser humano puede amar, es digno y se dignifica en este proceso.  Ningún otro
ser u objeto puede amar como lo hace el hombre. Somos seres únicos en este
mundo y tenemos un propósito por cumplimentar.

Por lo tanto, la riqueza y la pobreza son situaciones que nos creamos
precisamente en ese sentido del pensamiento. Podemos sentirnos ricos sin tener
grandes cosas o sentirnos pobres teniéndolo todo. Hay gente feliz gozando de lo
suficiente, otros, son terriblemente infelices buscando entre sus propiedades
algo que les genere placer. Pero, el mismo pensamiento de riqueza y apreciación
de ella nos lleva a escalar montañas más grandes y poco a poco ir soltando ese
pasado de mediocridad y miseria que no nos dignifican y que nada tiene que ver
con las cosas que poseemos. Tener mentalidad de campeón, ser alguien
productivo, amar a los demás y no dejar de cultivarse son signos de alguien que
tiene mentalidad de riqueza. Todo lo demás se da por añadidura.
