---
layout: post
title: "El nuevo cáncer que padece la tecnología"
date: "2020-09-21 22:43:47 -0500"
author: "César Ruvalcaba"
categories: ["Opinión"]
tags: ["Tecnología"]
excerpt: "La cultura tecnológica está siendo pisada y lo que ahora impera es una especie de «tianguis publicitario» más que una genuina praxis mercadológica. Las redes sociales más empleadas (mas no por ello más efectivas) se están saturando en efecto metastásico. Yo solo me cuestiono: ¿qué pasará con nuestra sociedad que piensa que solo por gestionar bien sus redes sociales ya es un experto en tecnología."
comments: false
---

[//]: # (Filename: 2020-09-21-el-nuevo-cancer-que-padece-la-tecnologia.markdown)
[//]: # (Author: César Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 21 sep 2020 22:45:30)
[//]: # (Last Modified: 04 oct 2020 12:25:20)

Procuraba mirar un vídeo en Youtube cuya duración no excedía los treinta
minutos. Pero en cada cierto intervalo me aparecía un vídeo publicitario.
Fueron uno, dos, cuatro, seis, y empecé a sentir desesperación. Al concluir uno
de ellos, se avecinaba otro de menor duración, en un fenómeno que denominé
"publicidad procrea publicidad". La experiencia fue tan terrible que se me
extinguieron las ganas de observar dicho vídeo completamente (ya que empecé a
dudar que esto acontecería).

Después, analicé que no solo ocurre en Youtube, también en páginas web o en los
primeros resultados de una búsqueda en Google, no aparecen más que anuncios
publicitarios. Esa oleada de promotores de la mercadotecnia no pasó
desapercibida, y ahora, es dificultoso el poder disfrutar de una página, o blog
o programa que esté limpio de este cáncer tecnológico. Todos quieren aparecer,
protagonizar, vender masivamente. La cultura tecnológica está siendo pisada y
lo que ahora impera es una especie de "tianguis publicitario" más que una
genuina praxis mercadológica. Las redes sociales más empleadas (mas no por ello
más efectivas) se están saturando en efecto metastásico.

Ahora se cree que saber tecnología es, sencillamente saber publicitar o manejar
las redes sociales, permitiendo entrever que la noble preocupación por los
lenguajes de programación, las matemáticas o la arquitectura de las
computadoras está pasando a ulteriores términos. Este fenómeno lamentable fue
caldo de cultivo para la mediocridad y la resistencia moderna a la
actualización, pues podemos ver a docentes que no saben manejar las clases en
línea, abogados que no dominan una audiencia online, padres de familia que se
quejan al no poder orientar a sus hijos en el manejo de las tecnologías,
teniendo bien en claro que las nuevas generaciones no recibirán la misma
educación que nosotros y que todo en adelante será ajustado a los sistemas de
cómputo y dispositivos electrónicos.

Yo solo me cuestiono: ¿qué pasará con nuestra sociedad que piensa que solo por
gestionar bien sus redes sociales ya es un experto en tecnología?
