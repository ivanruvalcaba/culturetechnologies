[//]: # (Filename: fundadores.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 17 sep 2020 23:57:56)
[//]: # (Last Modified: 18 sep 2020 11:08:57)

## Fundadores

---

![César Enrique Ruvalcaba Vargas](img/césar_ruvalcaba.png)

*Lic. César Enrique Ruvalcaba Vargas*

Conferencista, catedrático, filósofo, sociólogo. Además de ser periodista
cultural y tecnológico.

---

Ha publicado varios libros en su haber, destacando entre ellos: *«Sobre la
dignidad humana»* y *«Nocturnos»*.

---

![Mario Iván Ruvalcaba Vargas](img/iván_ruvalcaba.png)

*Mario Iván Ruvalcaba Vargas*

Nec arcu vel tellus tristique vestibulum. Aenean vel lacus. Mauris dolor erat,
commodo ut, dapibus vehicula, lobortis sit amet, orci. Aliquam augue.

---

Sed eleifend odio sed leo. Mauris tortor turpis, dignissim vel, ornare ac,
ultricies quis, magna. Phasellus lacinia, augue ac dictum tempor, nisi felis
ornare magna, eu vehicula tellus enim eu neque.
