[//]: # (Filename: introducción.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 17 sep 2020 13:14:47)
[//]: # (Last Modified: 18 sep 2020 00:38:19)

Fundada en el año 2020, **Culture & Technologies** es una *startup* mexicana
que, como su nombre indica, tiene como principal misión el *difundir la cultura
y avances tecnológicos* a las empresas y comunidad hispana, a través de
servicios de enseñanza a distancia, consultoría y gestión técnica.
