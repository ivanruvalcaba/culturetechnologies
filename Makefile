# Filename: Makefile
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 02 oct 2020 08:51:34
# Last Modified: 02 oct 2020 09:02:11
#
# Copyright (C) 2020  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

WIKI_DIR := _wiki
PUBLIC_WIKI_DIR := ../wiki

.PHONY: all
all: build

.PHONY: serve_wiki
serve:
	@echo "Running the server wiki..."
	mkdocs serve --config-file ${WIKI_DIR}/mkdocs.yml

.PHONY: build_wiki
build:
	@echo "Building the wiki site..."
	rm -rf ${PUBLIC_WIKI_DIR}
	mkdocs build --config-file ${WIKI_DIR}/mkdocs.yml --site-dir ${PUBLIC_WIKI_DIR}
