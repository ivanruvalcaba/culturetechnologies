# ¿Qué es Google Classroom?
Creado el sábado 03 de octubre del 2020

![](./Qué_es_Google_Classroom/google_classroom.png)

[Google Classroom](https://edu.google.com/intl/es/products/classroom/) es una plataforma basada en la web que permite a los instructores crear aulas en línea —virtuales— donde pueden crear, reunir y gestionar el trabajo de clase. También permite a los profesores transmitir y hacer equipo con los estudiantes y sus padres o tutores.

