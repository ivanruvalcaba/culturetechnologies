[//]: # (Filename: index.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 01 oct 2020 10:23:52)
[//]: # (Last Modified: 02 oct 2020 17:32:29)

# Inicio

![Culture & Technologies](culture_technologies.png)

Bienvenido a la wiki de **Culture & Technologies**. Aquí encontrará artículos y
documentación sobre conceptos o terminología relacionada con la cultura
informática.
