[//]: # (Filename: README.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <culturetechnologies[at]protonmail[dot]com>)
[//]: # (Created: 02 oct 2020 09:18:46)
[//]: # (Last Modified: 02 oct 2020 09:18:46)

# Culture & Technologies

Repositorio del sitio web de **Culture & Technologies**.

## Licencia

Copyright (c) 2020 — Iván Ruvalcaba. _A no ser que se indique explícitamente lo contrario, el contenido de este repositorio se encuentra sujeto a los términos de la licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional (CC BY-SA 4.0 International)](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES)._
